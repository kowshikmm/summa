# --- root/backend.tf ---

terraform {
  backend "s3" {
    bucket = "devops-22082022"
    key    = "remote.tfstate"
    region = "us-east-1"
  }
}
